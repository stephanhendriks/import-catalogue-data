## Usage:

- Set up your Moltin API keys in mymoltin.py

# Go to https://developers.google.com/drive/api/v3/quickstart/python and:
- Enable Drive API
- Download Client Configuration to this folder

# Run myapp.py (make sure you have the latest Python installed)

```
python3 myapp.py
```
You will have to check which dependencies are missing and install them with pip