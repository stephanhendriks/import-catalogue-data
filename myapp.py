from __future__ import print_function
import pickle, re, json, os.path, sys, os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from mymoltin import Moltin, deployNetlify
from myclasses import *

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SPREADSHEET_ID = '14xq4fAyYRmTcIvro3LzSrq9mCF-jjXxvrRJ6NKUiaU8'
SPREADSHEET_RANGE = 'Explanation Data'

moltin = Moltin()
moltin.authenticate()

def main():
  print("### Fetching sheet data and mapping to Moltin format...")
  sheet = fetchDriveData()
  if not sheet:
    print("Failed to fetch Google Sheet data")
    return

  products = list()
  state = 'heading'
  for row in sheet:
    # Capture column names
    if state == 'heading':
      keys = list(k.lower() for k in row)
      state = 'search'

    # Search for sections, create a new product
    elif state == 'search':
      if len(row) > 0:
        # Add a product with variations
        if "other" not in row[0].lower():
          product = Product()
          product.data['name'] = row[0]
          product.data['slug'] = slugify(row[0])
          product.data['tags'] = slugify(row[1])
          product.data['price'] = [{ "amount": intify(row[2]),
                                     "currency": "ZAR",
                                     "includes_tax": True }]
          product.data['description'] = row[4]
          if (row[5]):
            product.data['description'] = product.data['description'] + "{SPECS}" + row[5]
          if (row[6].lower() == 'yes'):
            product.data['manage_stock'] = True
          product.data['sku'] = row[7]
          products.append(product)
          state = 'variation'
        else:
          # Add normal products
          state = 'product'
      
    # Parse product options and variations
    elif state == 'variation':
      if len(row) > 0:
        if len(product.variations) == 0:
          variation = Variation()
          variation.data['name'] = product.data['name'] + " Variations"
          product.variations.append(variation)

        # Add new option
        option = Option()
        option.data['name'] = row[0]
        option.data['description'] = row[0]
        variation.options.append(option)

        # Add all modifiers for option
        modifier = Modifier()
        modifier.data['modifier_type'] = 'name_equals'
        modifier.data['value'] = row[0]
        option.modifiers.append(modifier)

        modifier = Modifier()
        modifier.data['modifier_type'] = 'slug_equals'
        modifier.data['value'] = slugify(row[0])
        option.modifiers.append(modifier)

        modifier = Modifier()
        modifier.data['modifier_type'] = 'price_equals'
        modifier.data['value'] = [{ "amount": intify(row[2]),
                                    "currency": "ZAR",
                                    "includes_tax": True }]
        option.modifiers.append(modifier)

        modifier = Modifier()
        modifier.data['modifier_type'] = 'description_equals'
        modifier.data['value'] = row[4]
        if (row[5]):
          modifier.data['value'] = modifier.data['value'] + "{SPECS}" + row[5]
        option.modifiers.append(modifier)

        modifier = Modifier()
        modifier.data['modifier_type'] = 'sku_equals'
        modifier.data['value'] = row[7]
        option.modifiers.append(modifier)
      else:
        state = 'search'
      
    # Parse single product
    elif state == 'product':
      product = Product()
      if len(row) > 1:
          product = Product()
          product.data['name'] = row[0]
          product.data['slug'] = slugify(row[0])
          product.data['tags'] = slugify(row[1])
          product.data['price'] = [{ "amount": intify(row[2]),
                                     "currency": "ZAR",
                                     "includes_tax": True }]
          product.data['description'] = row[4]
          if (row[5]):
            product.data['description'] = product.data['description'] + "{SPECS}" + row[5]
          if (row[6].lower() == 'yes'):
            product.data['manage_stock'] = True
          product.data['sku'] = row[7]
          products.append(product)
      else:
        state = 'search'

  # Fetches Moltin products and stores them as moltin.allProducts
  print("### Fetching Moltin products...")
  moltin.getProducts()

  print("### Checking which items are updateable...")
  # Check if singular products exist and need updates
  for newProduct in products:
    for oldProduct in moltin.allProducts:
      # Child products should be ignored, they are generated from parents
      if 'parent' in oldProduct['relationships']:
        continue

      # Product already exists
      if (newProduct.data['slug'] == oldProduct['slug'] or
          newProduct.data['sku'] == oldProduct['sku']):

        # Product exists, add existing id
        newProduct.data['id'] = oldProduct['id']

        # Variation exists, add existing id
        if newProduct.variations and 'variations' in oldProduct['meta']:
          newVariation = newProduct.variations[0]
          oldVariation = oldProduct['meta']['variations'][0]
          newVariation.data['id'] = oldVariation['id']

          # Check options for existing
          for newOption in newVariation.options:
            for oldOption in oldVariation['options']:
              # Option exists, add existing id
              if (newOption.data['name'] == oldOption['name'] or 
                  newOption.data['description'] == oldOption['description'] or
                 (len(newVariation.options) == 1 and len(oldVariation['options']) == 1) ):
                newOption.data['id'] = oldOption['id']

                modReqResult = moltin.getModifiers(oldVariation, oldOption)
                oldModifiers = modReqResult if modReqResult else []
                # Check modifiers for existing
                for newModifier in newOption.modifiers:
                  for oldModifer in oldModifiers:
                    # Modifier exists, add existing id
                    if newModifier.data['modifier_type'] == oldModifer['modifier_type']:
                      newModifier.data['id'] = oldModifer['id']
                      if newModifier.data['value'] == oldModifer['value']:
                        del newModifier.data['value']
                      break

                # Remove all invalidated modifiers, if all of them delete the option too
                newOption.modifiers = [mod for mod in newOption.modifiers if 'value' in mod.data]
                if ( len(newOption.modifiers) == 0 and
                     newOption.data['name'] == oldOption['name'] and
                     newOption.data['description'] == oldOption['description'] ):
                  del newOption.data['name']
                break

          # Remove all invalidated options, if all of them delete the variation too
          newVariation.options = [opt for opt in newVariation.options if 'name' in opt.data]
          if ( len(newVariation.options) == 0 and
              newVariation.data['name'] == oldVariation['name'] ):
            del newVariation.data['name']

          # Remove all invalidated variations
          newProduct.variations = [var for var in newProduct.variations if 'name' in var.data]

        # Delete fields that are already identical or empty
        for key in oldProduct:
          if key in newProduct.data:
            # Minimum fields for a valid post
            if key == 'name' or key == 'type' or key == 'id':
              continue
            elif key == 'price':
              if newProduct.data['price'][0]['amount'] == oldProduct['price'][0]['amount']:
                del newProduct.data['price']
            elif (newProduct.data[key] == oldProduct[key] or newProduct.data[key] == ''):
              del newProduct.data[key]

        # Remove name field if everything is unchanged
        if ( len(newProduct.variations) == 0 and
            len(newProduct.data) == 3 and 
            newProduct.data['name'] == oldProduct['name'] ):
          del newProduct.data['name']
          
        break

  # Finally sanitize products
  products = [prod for prod in products if 'name' in prod.data]
  printStructure(products)

  # Run script with --update to push all updates live to Moltin
  if ('--update' in sys.argv or '--all' in sys.argv):
    print("### Posting new data to Moltin...")
    # Update if id was linked, otherwise post new items
    for product in products:
      product.data = dict((k, v) for k,v in product.data.items() if v != "")
      if 'id' in product.data:
        moltin.updateProduct(product.data)
      elif ('--no-create' not in sys.argv):
        print("Posting new product: " + product.data['name'])
        moltin.postProduct(product.data)
      else:
        print("No identifier found to update product: " + product.data['name'])

      for variation in product.variations:
        variation.data = dict((k, v) for k,v in variation.data.items() if v != "")
        if 'id' in variation.data:
          moltin.updateVariation(variation.data)
        elif ('--no-create' not in sys.argv):
          moltin.postVariation(variation.data)
          moltin.postVariationRelationship(product.data, variation.data)
          print("Posting new variation: " + variation.data['name'])
        else:
          print("No identifier found to update variation: " + variation.data['name'])

        for option in variation.options:
          option.data = dict((k, v) for k,v in option.data.items() if v != "")
          if 'id' in option.data:
            moltin.updateOption(variation.data, option.data)
          elif ('--no-create' not in sys.argv):
            moltin.postOption(variation.data, option.data)
            print("Posting new option: " + option.data['name'])
          else:
            print("No identifier found to update option: " + option.data['name'])

          for modifier in option.modifiers:
            modifier.data = dict((k, v) for k,v in modifier.data.items() if v != "")
            if 'id' in modifier.data:
              moltin.updateModifier(variation.data, option.data, modifier.data)
            elif ('--no-create' not in sys.argv):
              moltin.postModifier(variation.data, option.data, modifier.data)
              print("Posting new modifier: " + modifier.data['modifier_type']
                  + " for product " + product.data['name'])
            else:
              print("No identifier found to update modifier: " + modifier.data['modifier_type']
                  + " for product " + product.data['name'])

  # Run script with --build to rebuild child variations  
  if ('--build' in sys.argv or '--update' in sys.argv or '--all' in sys.argv):
    buildProducts(products)

  # Run script with --upload-images upload images in kooi-images folder
  if ('--upload-images' in sys.argv or '--all' in sys.argv):
    uploadImages()

  # Run script with --assign-images to push and relate images
  if ('--assign-images' in sys.argv or '--all' in sys.argv):
    assignImages()

  # Run script with --deploy to trigger the Netlify build hook
  if ('--deploy' in sys.argv or '--all' in sys.argv):
    print("### Netlify webhook triggering...")
    deployNetlify()

# This function simply matches product skus to filenames and then assigns them to products
def assignImages():
  print("### Matching products with images...")
  moltin.getFiles()
  moltin.getProducts()

  # Create a structure for us to use, and verify current file ID's are valid
  for product in moltin.allProducts:
    product['fileRelationships'] = []

  # TODO: Should check here to avoid assigning where already assigned
  for mfile in moltin.allFiles:
    for product in moltin.allProducts:
      if (mfile['file_name'].split('_')[0] in product['sku'] or mfile['file_name'].split('.')[0] in product['sku']):
        product['fileRelationships'].append(mfile)

  for product in moltin.allProducts:
    mainFile = ''
    otherFiles = []
    # loop and find a main image for us
    for oneFile in product['fileRelationships']:
      if ( (len(product['fileRelationships']) == 1 or
            oneFile['file_name'].split('.')[0] in product['sku'] or
            oneFile['file_name'].split('_')[1].split('.')[0] == '1') and
          mainFile == ''):
        mainFile = oneFile
      else:
        otherFiles.append(oneFile)

    # hopefully we found one file that can act as main image
    if mainFile != '':
      moltin.postMainFileRelationship(product, mainFile)
      for oneFile in otherFiles:
        moltin.postFileRelationship(product, oneFile)
    else:
      print("ERROR: No  main image found to match " + product['name'])

  # Now that we made lots of changes to image relationships, go check them all to delete broken links
  checkRelationships()

# Upload new files in kooi-images folder
def uploadImages():
  print("### Deleting all images on Moltin...")
  moltin.getFiles()

  # First delete everything there and start clean
  for oneFile in moltin.allFiles:
    moltin.deleteFile(oneFile)
    
  # Upload files one by one
  imagePath = os.path.join(os.getcwd(), "kooi-images")
  for filename in os.listdir(imagePath):
    if (filename.endswith(".jpg") or filename.endswith(".jpeg") or filename.endswith(".png") or filename.endswith(".gif")):
      print("Posting file: " + filename)
      moltin.postFile(os.path.join(imagePath, filename))

# Check file relationships to make sure they are valid. Thanks Moltin, for creating broken
# relationships that we have to fix ourselves
def checkRelationships():
  moltin.getProducts()

  # Iterate and verify current file ID's are valid
  for product in moltin.allProducts:
    if 'main_image' in product['relationships']:
      fileItem = product['relationships']['main_image']['data']
      # Product has a main image. Let's make sure it exists
      resp = moltin.getFile(fileItem)
      if (not resp):
        moltin.deleteMainFileRelationship(product, fileItem)

    if 'files' in product['relationships']:
      # Product has other images. Let's make sure they exists
      for fileItem in product['relationships']['files']['data']:
        resp = moltin.getFile(fileItem)
        if (not resp):
          moltin.deleteFileRelationship(product, fileItem)

# This function fetches all data from the Google Sheet and returns it as a list of lists
def fetchDriveData():
  """ Fetch sheet data from Google Drive """
  creds = None
  # The file token.pickle stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
      creds = pickle.load(token)
  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
      creds.refresh(Request())
    else:
      flow = InstalledAppFlow.from_client_secrets_file(
        'credentials.json', SCOPES)
      creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
      pickle.dump(creds, token)

  service = build('sheets', 'v4', credentials=creds)

  # Call the Sheets API
  sheet = service.spreadsheets()
  result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                              range=SPREADSHEET_RANGE).execute()
  values = result.get('values', [])

  if not values:
    print('No data found.')
  else:
    return values 

# This function builds all the child products from parent products on Moltin
def buildProducts(products = 'none given'):
  if (products == 'none given'):
    moltin.getProducts()
    print("### Building existing child products on Moltin...")
    for product in moltin.allProducts:
      if 'variations' in product['meta']:
        moltin.buildVariationData(product)
  else:
    print("### Building new child products on Moltin...")
    for product in products:
      # Build child products if there are variations
      if len(product.variations) > 0:
        moltin.buildVariationData(product.data)

# Helper function to print out the current structure of all products
def printStructure(products):
  for product in products:
    lastpart = (' - ' + product.data['id']) if 'id' in product.data else ''
    print("PRO: " + product.data['name'] + lastpart)
    for variation in product.variations:
      lastpart = (' - ' + variation.data['id']) if 'id' in variation.data else ''
      print("  VAR: " + variation.data['name'] + lastpart)
      for option in variation.options:
        lastpart = (' - ' + option.data['id']) if 'id' in option.data else ''
        print("    OPT: " + option.data['name'] + lastpart)
        for modifier in option.modifiers:
          lastpart = (' - ' + modifier.data['id']) if 'id' in modifier.data else ''
          print("      MOD: " + modifier.data['modifier_type'] + lastpart)

if __name__ == '__main__':
  if ('--assign-only' in sys.argv):
    assignImages()
    exit()
  if ('--check-only' in sys.argv):
    checkRelationships()
    exit()
  if ('--build-only' in sys.argv):
    buildProducts()
    exit()
  if ('--upload-only' in sys.argv):
    uploadImages()
    assignImages()
    exit()
  main()