import requests, json, copy, os

class Moltin():
  # Moltin URLs
  MOLTIN_API = 'https://api.moltin.com/v2/'
  MOLTIN_AUTH = 'https://api.moltin.com/oauth/access_token'
  MOLTIN_TOKEN = ''
  POST_HEADERS = dict()
  FILE_POST_HEADERS = dict()
  GET_HEADERS = dict()
  FILE_HEADERS = dict()

  allProducts = list()
  allVariations = list()
  allModifiers = list()
  allFiles = list()

  def authenticate(self):
    # Moltin token
    response = requests.post(self.MOLTIN_AUTH,
      data = {
        "grant_type" : "client_credentials",
        "client_secret" : "TbtU97dCcvLb55i2eWOlcDFvyUXau5xJ66aBU8pAdU",
        "client_id" : "dPZGE33omAlsSPkUGZGiLNpwuDGbQ7Z2MYcRbmSRGy"
      }
    )
    if response:
      self.MOLTIN_TOKEN = response.json()['access_token']
      self.POST_HEADERS = { "Authorization" : ("Bearer " + self.MOLTIN_TOKEN ),
                            "Content-Type"  :  "application/json"}
      self.FILE_POST_HEADERS = { "Authorization" : ("Bearer " + self.MOLTIN_TOKEN )}
      self.GET_HEADERS  = { "Authorization" : ("Bearer " + self.MOLTIN_TOKEN )}
      self.FILE_HEADERS = { "Authorization" : ("Bearer " + self.MOLTIN_TOKEN ),
                            "Content-Type"  :  "multipart/form-data"}
    else:
      print("Moltin Auth failed: " + str(response.text))

  #### Post single items
  # Post a single product
  def postProduct(self, product):
    resp = self.moltinPost('products', product)
    if resp:
      print("Product posted: " + product['name'])
      product['id'] = resp.json()['data']['id']
      return resp

  # Post a single variation
  def postVariation(self, variation):
    resp = self.moltinPost('variations', variation)
    if resp:
      print("Variation posted: " + variation['name'])
      variation['id'] = resp.json()['data']['id']
      return resp

  # Post a single modifier
  def postModifier(self, variation, option, modifier):
    resp = self.moltinPost('variations/' + variation['id'] + '/options/' + option['id'] + '/modifiers', modifier)
    if resp:
      print("Modifier posted: " + modifier['modifier_type'])
      modifier['id'] = resp.json()['data']['id']
      return resp

  # Post a single option
  def postOption(self, variation, option):
    resp = self.moltinPost('variations/' + variation['id'] + '/options', option)
    if resp:
      print("Option posted: " + option['name'])
      option['id'] = resp.json()['data']['id']
      return resp

  # Post files
  def postFile(self, filePath):
    resp = self.moltinPostFile('files', {
      'file': (os.path.basename(filePath), open(filePath, 'rb'))
    })
    if resp:
      print("File posted successfully")
      return resp

  #### Post Relationships
  # Post a variation relationship
  def postVariationRelationship(self, product, variation):
    resp = self.moltinPost('products/' + product['id'] + '/relationships/variations',[{
      "type": "product-variation",
      "id": variation['id']
    }])
    if resp:
      print("Variation relationship posted: " + variation['name'])
      return resp

  # Post a main file (main image) relationship
  def postMainFileRelationship(self, product, image):
    # a PUT request will OVERWRITE all existing relationships
    resp = self.moltinPut('products/' + product['id'] + '/relationships/main-image',{
      'type': 'main_image',
      'id'  : image['id']
    })
    if resp:
      print("Main image relationship posted: " + product['name'])

  # Post a regular file (image) relationship
  def postFileRelationship(self, product, image):
    # the POST request will add to existing relationships
    resp = self.moltinPost('products/' + product['id'] + '/relationships/files',[{
      'type': 'file',
      'id'  : image['id']
    }])
    if resp:
      print("Extra image relationship posted: " + product['name'])

  #### Update single item
  # Update a single product
  def updateProduct(self, product):
    print("Product update: " + product['name'])
    postData = product
    resp = self.moltinPut('products/' + postData['id'], postData)
    if resp:
      return resp
    else:
      print("Update product failed: " + postData['id'])
      exit()

  # Update a single variation
  def updateVariation(self, variation):
    print("Variation update: " + variation['name'])
    postData = variation
    resp = self.moltinPut('variations/' + postData['id'], postData)
    if resp:
      return resp
    else:
      print("Update variation failed: " + postData['id'])
      exit()

  # Update a single option
  def updateOption(self, variation, option):
    print("Option update: " + option['name'])
    postData = option
    resp = self.moltinPut('variations/' + variation['id'] + '/options/' + postData['id'], postData)
    if resp:
      return resp
    else:
      print("Update option failed: " + postData['id'])
      exit()

  # Update a single modifier
  def updateModifier(self, variation, option, modifier):
    print("Modifier update: " + modifier['modifier_type'])
    postData = modifier
    resp = self.moltinPut('variations/' + variation['id'] + '/options/' + option['id'] + '/modifiers/' + postData['id'], postData)
    if resp:
      return resp
    else:
      print("Update modifier failed: " + postData['id'])
      exit()

  #### Get items
  # Get all products (variations are included in this product object)
  def getProducts(self):
    resp = self.moltinGet('products')
    if resp:
      self.allProducts = resp.json()['data']
      return self.allProducts

  # Get all modifiers
  def getModifiers(self, variation, option):
    resp = self.moltinGet('variations/'+ variation['id'] +'/options/'+ option['id'] +'/modifiers')
    if resp:
      if isinstance(resp.json(), list):
        return []
      return resp.json()['data']

  # Get all files
  def getFiles(self):
    resp = self.moltinGet('files')
    if resp:
      print("All files fetched")
      self.allFiles = resp.json()['data']
      return resp.json()['data']

  # Get a single file
  def getFile(self, fileVar):
    resp = self.moltinGet('files/' + fileVar['id'])
    if resp:
      print("Product file fetched: " + fileVar['id'])
      self.allFiles = resp.json()['data']
      return resp.json()['data']
    else:
      print("File ID does not exist: " + fileVar['id'])
      return None

  #### Delete items
  # Delete an image/file
  def deleteFile(self, fileVar):
    resp = self.moltinDelete('files/' + fileVar['id'], )
    if resp:
      print("Deleted file: " + fileVar['file_name'])
      return resp.text

  # Delete an image/file relationship
  def deleteFileRelationship(self, product, fileVar):
    resp = self.moltinDelete('products/' + product['id'] + '/relationships/files',[{
      'type': 'file',
      'id'  : fileVar['id']
    }])
    if resp:
      print("Deleted file relationship: " + product['name'])
      return resp.text

  # Delete a main-image relationship
  def deleteMainFileRelationship(self, product, fileVar):
    resp = self.moltinDelete('products/' + product['id'] + '/relationships/main-image',{
      'type': 'file',
      'id'  : fileVar['id']
    })
    if resp:
      print("Deleted main-image relationship" + product['name'])
      return resp.text

  #### Builder functions
  # Builder function to fetch all variation data of a product
  def buildVariationData(self, product):
    if type(product) is dict:
      prodId = product['id']
    else:
      prodId = product.data['id']
    print("Building " + prodId)
    resp = self.moltinPost('products/' + prodId + '/build')
    if resp:
      return resp
    else:
      print("BUILD variations failed")
      return None

  #### Helper functions
  # Helper function to simplify post file
  def moltinPostFile(self, urlAppend, files ={}):
    resp = requests.post(self.MOLTIN_API + urlAppend,
                          headers=self.FILE_POST_HEADERS,
                          data={ 'public': 'true' },
                          files=files)

    if (resp.status_code != 201 and resp.status_code != 200):
      print("POST file failed: " + str(resp.text))
      print(resp.text)

    if resp:
      return resp
    else:
      return None

  # Helper function to simplify posts
  def moltinPost(self, urlAppend, data={}, files ={}):
    resp = requests.post(self.MOLTIN_API + urlAppend,
                          headers=self.POST_HEADERS,
                          data=json.dumps({"data" :data}),
                          files=files)

    if (resp.status_code != 201 and resp.status_code != 200):
      print("POST failed: " + str(resp.text))
      print(resp.text)

    if resp:
      return resp
    else:
      return None

  # Helper function to simplify get
  def moltinGet(self, urlAppend):
    resp = requests.get(self.MOLTIN_API + urlAppend,
                          headers=self.GET_HEADERS)

    if (resp.status_code != 201 and resp.status_code != 200):
      print("GET failed: " + str(resp.text))
      print(resp.text)

    if resp:
      return resp
    else:
      return None

  # Helper function to simplify put
  def moltinPut(self, urlAppend, data):
    resp = requests.put(self.MOLTIN_API + urlAppend,
                          headers=self.POST_HEADERS,
                          data=json.dumps({"data" :data}))
                          
    if (resp.status_code != 201 and resp.status_code != 200):
      print("PUT failed: " + str(resp.text))
      print(resp.text)
      
    if resp:
      return resp
    else:
      return None

  # Helper function to simplify delete
  def moltinDelete(self, urlAppend, data={}):
    resp = requests.delete(self.MOLTIN_API + urlAppend,
                          headers=self.POST_HEADERS,
                          data=json.dumps({"data" :data}))
                          
    if (resp.status_code != 201 and resp.status_code != 200 and resp.status_code != 204):
      print("DELETE failed: " + str(resp.text))
      print(resp.text)
      
    if resp:
      return resp
    else:
      return None

def deployNetlify():
  resp = requests.post("https://api.netlify.com/build_hooks/5cd5114a66400207c7557bc2")
  if resp:
    print("Netlify build triggered")
    return resp
  else:
    print("NETLIFY build hook failed: " + str(resp.text))
    return None
