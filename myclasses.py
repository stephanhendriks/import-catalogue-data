import re

# Moltin types with minimum required data
class Product():
  def __init__(self):
    self.data = dict({
      "name": "",
      "slug": "",
      "sku": "",
      "description": "",
      "price": [{
        "amount": 0,
        "currency": "ZAR",
        "includes_tax": True
      }],
      "manage_stock": False,
      "status": "live",
      "commodity_type": "physical",
      "type": "product"
    })
    self.variations = list()
    self.fileRelationships = list()

class Variation():
  def __init__(self):
    self.data = dict({
        "name": "",
        "type": "product-variation"
      })
    self.options = list()

class Option():
  def __init__(self):
    self.data = dict({
		    "name": "",
        "description": "",
		    "type": "product-variation-option"
        })
    self.modifiers = list()

class Modifier():
  def __init__(self):
    self.data = dict({
      "modifier_type": "",
      "value": "",
      "type": "modifier"
      })

# Helper function to slugify
def slugify(oldString):
  return re.sub(r'(-)\1+', r'\1', oldString.lower().strip().replace(' ','-'))

# Helper function to intify
def intify(oldString):
  if oldString != '':
    return int(re.sub(r"\D", "", oldString))
  else:
    return 0